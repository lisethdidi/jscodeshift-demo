import * as lib from "./lib";

function transform(file, api) {
  const j = api.jscodeshift;
  const root = j(file.source);

  const hocMethodNames = [
    "restartBreadCrumb",
    "pushBreadCrumb",
    "replaceBreadCrumb",
    "setBreadCrumbs"
  ];

  // ------------------------------------------------------------------------------------------------
  // Replace import of old breadcrumb hoc with new breadcrumbBar object
  //
  // Change:  `import { injectBreadCrumb, InjectedBreadCrumbProps } from "src/chrome/hoc/breadcrumb";`
  // To:      `import { breadCrumbBar } from "src/chrome";`
  // ------------------------------------------------------------------------------------------------
  const imports = root.find(j.ImportDeclaration, {
    source: {
      value: v => /chrome\/hoc\/breadcrumb/.test(v)
    }
  });
  if (imports.length) {
    api.report("Replacing legacy hoc import");
    imports.replaceWith(lib.createImport("breadCrumbBar", "src/chrome"));
  } else {
    return null;
  }

  // ------------------------------------------------------------------------------------------------
  // Remove `InjectedBreadCrumbProps` interface references
  //
  // Change:  `export interface Props extends InjectedIntlProps, InjectedBreadCrumbProps {}`
  // To:      `export interface Props extends InjectedIntlProps {}`
  // ------------------------------------------------------------------------------------------------
  const interfaceRefs = root.find(j.Identifier, {
    name: "InjectedBreadCrumbProps"
  });
  if (interfaceRefs.length) {
    api.report("Removing InjectedBreadCrumbProps interface reference");
    interfaceRefs.forEach(p => p.parent.prune());
  }

  // Remove `injectBreadCrumb()` HOC calls
  const injectCalls = root.find(j.CallExpression).filter(c => {
    return c.node.callee && c.node.callee.name === "injectBreadCrumb";
  });
  if (injectCalls.length) {
    api.report(`Removing ${injectCalls.length} calls to injectBreadCrumb()`);
    injectCalls.forEach(c => {
      if (c.parent.node.type === "CallExpression") {
        c.parent.node.arguments = c.node.arguments;
      } else if (c.parent.node.type === "ExportDefaultDeclaration") {
        c.parent.node.declaration = c.node.arguments[0];
      } else if (c.parent.node.type === "VariableDeclarator") {
        c.parent.node.init = c.node.arguments[0];
      } else {
        throw new Error(
          `Unable to prune injectBreadCrumb(): parent node type is ${
            c.parent.node.type
          }`
        );
      }
    });
  }

  // ------------------------------------------------------------------------------------------------
  // Remove method calls to legacy HOC props, replace with one call to `breadCrumbBar.set()`
  // ------------------------------------------------------------------------------------------------
  const legacyHocIdents = root
    .find(j.Identifier)
    .filter(p => hocMethodNames.includes(p.node.name));
  if (legacyHocIdents.length) {
    api.report(
      `Removing ${legacyHocIdents.length} references to legacy HOC props`
    );

    // Keep track of where to insert new call to `breadCrumbBar.set()`
    let insertionPoint = null;

    // Keep track of old arguments passed to legacy HOC methods
    const oldArgs = [];

    legacyHocIdents.forEach(p => {
      if (!insertionPoint) {
        if (p.parent.node.type !== "MemberExpression") {
          insertionPoint = p.parent.parent;
        } else {
          insertionPoint = p.parent.parent.parent;
        }
      }

      const keepArgs = ["componentWillMount", "constructor"].includes(
        lib.parentClassMethodName(p)
      );

      if (p.parent.node.type !== "MemberExpression") {
        if (keepArgs) {
          oldArgs.push(p.parent.node.arguments);
        }
        p.parent.prune();
      } else {
        if (keepArgs) {
          oldArgs.push(p.parent.parent.node.arguments);
        }

        const parentIf = j(p).closest(j.IfStatement);
        if (parentIf.length) {
          parentIf.replaceWith(null);
        } else {
          p.parent.parent.prune();
        }
      }
    });

    const newExpression = () => {
      const args = [];

      // Transform old arg lists
      //
      // (intl.formatMessage(msg.pageTitle)) => ({ label: intl.formatMessage(msg.pageTitle) })
      oldArgs.forEach(oldArgList => {
        if (!oldArgList.length) {
          return;
        }

        const firstArg = oldArgList[0];
        const secondArg = oldArgList[1];

        // Handle calls to setBreadCrumbs where arg is an array of objects
        if (firstArg.type === "ArrayExpression") {
          firstArg.elements.forEach(arg => {
            if (arg.type !== "ObjectExpression") {
              return;
            }

            arg.properties.forEach(prop => {
              if (prop.key.name === "text") {
                prop.key.name = "label";
              }

              if (prop.key.name === "url") {
                prop.key.name = "to";
              }
            });

            args.push(arg);
          });

          return;
        }

        const properties = [j.property("init", j.literal("label"), firstArg)];

        if (secondArg) {
          properties.push(j.property("init", j.literal("to"), secondArg));
        }

        args.push(j.objectExpression(properties));
      });

      return j.expressionStatement(
        j.callExpression(
          j.memberExpression(
            j.identifier("breadCrumbBar"),
            j.identifier("set")
          ),
          [j.arrayExpression(args)]
        )
      );
    };

    insertionPoint.insertAfter(newExpression());
  }

  // ------------------------------------------------------------------------------------------------
  // Ensure breadCrumbBar.clear() is called from componentWillUnmount()
  // ------------------------------------------------------------------------------------------------
  const compWillUnmount = root.find(j.ClassMethod, {
    key: { name: "componentWillUnmount" }
  });
  if (!compWillUnmount.length) {
    // insert componentWillUnmount() lifecycle method and call breadCrumbBar.clear() inside
    const methodStr =
      "public componentWillUnmount() { breadCrumbBar.clear(); }";

    api.report("Inserting componentWillUnmount()");
    root
      .find(j.ClassDeclaration)
      .get(0)
      .node.body.body.push(methodStr);
  } else {
    api.report("Updating componentWillUnmount()");

    const callClear = j.expressionStatement(
      j.callExpression(
        j.memberExpression(
          j.identifier("breadCrumbBar"),
          j.identifier("clear")
        ),
        []
      )
    );
    compWillUnmount.get(0).node.body.body.push(callClear);
  }

  return root.toSource();
}

module.exports = transform;
module.exports.parser = "tsx";
