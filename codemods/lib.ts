import * as util from "util";
import j from "jscodeshift";

/*tslint:disable no-console*/
export const inspect = (obj, reFilter?) => {
  const out = util
    .inspect(obj, true, 0, true)
    .split(",")
    .sort()
    .filter(k => {
      if (reFilter) {
        return reFilter.test(k);
      }

      return true;
    })
    .join(",");

  console.log(out);
};

// Create a simple import statement
// Examples:
//
//   createImport("abc", "src/foo") => `import { abc } from "src/foo";`
//   createImport("abc", "xyz", "src/foo") => `import { abc, xyz } from "src/foo";`
export const createImport = (...args: string[]) => {
  if (args.length < 2) {
    throw new Error("At least two args required");
  }

  const names = args.slice(0, args.length - 1);
  const path = args[args.length - 1];

  return j.importDeclaration(
    names.map(n => j.importSpecifier(j.identifier(n))),
    j.literal(path)
  );
};

// Get closest class method name to given path
//
// Example source:
//  class Foo {
//      public bar() {
//          const age = 1;
//      }
//  }
//
// Say you have NodePath p which refers to the Identifier "age".
//
// parentClassMethodName(p) would return "bar"
export const parentClassMethodName = p => {
  let name;

  try {
    name = j(p)
      .closest(j.ClassMethod)
      .get(0).node.key.name;
  } catch (e) {
    name = null;
  }

  return name;
};
