import { describe } from "jscodeshift-helper";

function transform(file, api) {
  const j = api.jscodeshift;
  const root = j(file.source);

  api.report("Checking...");

  root.find(j.Identifier).replaceWith(p => ({
    ...p.node,
    name: p.node.name
      .split("")
      .reverse()
      .join("")
  }));
  return root.toSource();
}

module.exports = transform;
module.exports.parser = "tsx";
