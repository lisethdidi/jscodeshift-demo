import {
  injectBreadCrumb,
  InjectedBreadCrumbProps
} from "src/chrome/hoc/breadcrumb";

export interface Props extends WithAuthProps, InjectedBreadCrumbProps {}

export class HomePage extends Component<Props, object> {
  constructor(props: Props) {
    super(props);
    this.props.restartBreadCrumb([
      { text: "Home", url: "/home" },
      { text: "Login" }
    ]);
  }

  render() {
    return <div>Login Page!</div>;
  }

  componentWillUnmount() {
    abc.unsubscribe();
  }
}

export default withAuth(injectBreadCrumb(HomePage));
