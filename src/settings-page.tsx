import {
  injectBreadCrumb,
  InjectedBreadCrumbProps
} from "src/chrome/hoc/breadcrumb";

export interface Props extends WithAuthProps, InjectedBreadCrumbProps {}

export class SettingsPage extends Component<Props, object> {
  constructor(props: Props) {
    super(props);
    this.props.setBreadCrumbs([{ text: "Settings" }]);
  }

  render() {
    return <div>Settings Page!</div>;
  }
}

export default withAuth(injectBreadCrumb(SettingsPage));
