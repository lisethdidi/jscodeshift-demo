.PHONY: demo1 demo2 explore

demo1:
	yarn jscodeshift -t codemods/reverse.ts src/*

demo2:
	yarn jscodeshift -t codemods/breadcrumb.ts src/* -d --print

astexplorer:
	git clone https://github.com/fkling/astexplorer.git
	cd astexplorer/website && yarn add typescript@3.2.2 && yarn add jscodeshift@0.6.2 && yarn build

explore: astexplorer
	cd astexplorer/website && yarn start
